<?xml version="1.0" encoding="UTF-8"?>
<!--
Big'uns
Warhammer Online: Age of Reckoning UI modification that fixes the
Witch Elf class icon.
    
Copyright (C) 2008  Dillon "Rhekua" DeLoss
rhekua@msn.com		    www.rhekua.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="Big'uns" version="1.1" date="7/2/2009" >
		<VersionSettings gameVersion="1.3.0" windowsVersion="1.0" savedVariablesVersion="1.0" />
		<Author name="Rhekua" email="rhekua@msn.com" />
		<Description text="Fixes the Witch Elf class icon." />
		<Files>
			<File name="Big'uns.lua" />
			<File name="Big'uns.xml" />
		</Files>
		<OnInitialize>
			<CallFunction name="Biguns.OnInitialize" />
		</OnInitialize>
		<OnShutdown>
			<CallFunction name="Biguns.OnShutdown" />
    		</OnShutdown>
	</UiMod>
</ModuleFile>
