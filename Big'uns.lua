--[[
Big'uns
Warhammer Online: Age of Reckoning UI modification that fixes the
Witch Elf class icon.
    
Copyright (C) 2008  Dillon "Rhekua" DeLoss
rhekua@msn.com		    www.rhekua.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
]]--

Biguns = {}

local hDynamicImageSetTexture = nil
local hCircleImageSetTexture = nil

function Biguns.OnInitialize()
	hDynamicImageSetTexture = DynamicImageSetTexture
	DynamicImageSetTexture = Biguns.DynamicImageSetTexture

	hCircleImageSetTexture = CircleImageSetTexture
	CircleImageSetTexture = Biguns.CircleImageSetTexture
end

function Biguns.OnShutdown()
	DynamicImageSetTexture = hDynamicImageSetTexture
	hDynamicImageSetTexture = nil

	CircleImageSetTexture = hCircleImageSetTexture
	hCircleImageSetTexture = nil
end

function Biguns.DynamicImageSetTexture(image, texture, x, y)

	if ( texture == "icon020201" ) then
		texture = "icon0202011"
	end

	return hDynamicImageSetTexture(image, texture, x, y)
end

function Biguns.CircleImageSetTexture(image, texture, x, y)

	if ( texture == "icon020201" ) then
		texture = "icon0202011"
	end

	return hCircleImageSetTexture(image, texture, x, y)
end